NZ NEWS APP
===========

Made this app to put it up on the app store but Apple kept rejecting it as it had news pertaining to COVID :|.

So I am releasing the source code so that anyone can build and install it on thier on their phone.

It is a simple app that uses NEWSAPI to get the latest news from a specific country ( New Zealand in this case). You get all the latest news from all the major news sources in one place instead of hopping to the different news providers.

``` swift
// In ApiCaller.swift

        // For all Top Headlines
        var urlString =  "https://newsapi.org/v2/top-headlines?country=\(country)&pagesize=\(pageSize)&page=\(page)&apiKey=660fe4700bd34641aa93bcc5b3ed38ee"
        
        // All News without COVID in the news

        //var urlString = "https://newsapi.org/v2/everything?q=-covid&pagesize=\(pageSize)&page=\(page)&apiKey=660fe4700bd34641aa93bcc5b3ed38ee"
        
        // All news from newshub, stuff and rnz without COVID or OMICRON in the headline
        /*
        var urlString = "https://newsapi.org/v2/everything?q=-coronavirus%20AND%20-omicron%20AND%20-covid&domains=newshub.co.nz,stuff.co.nz,rnz.co.nz&apiKey=660fe4700bd34641aa93bcc5b3ed38ee&pagesize=\(pageSize)&page=\(page)"
        */


```

Features
========
- Set the country and the news agencies you want to get the news from (Set to NZ).
- Pull Down to Refresh the news.
- Pagination to add 10 new news headings on each page when you pull up at the end of the screen.
- Includes google-ads. 
- In-App purchase to remove the Ad. 
- Buttons to Purchase and Restore the purchase.



Screenshot
==========
<img src="screenshot/ss.png" width="48"/>