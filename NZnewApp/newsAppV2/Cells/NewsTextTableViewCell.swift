//
//  NewsTextTableViewCell.swift
//  newsAppV2
//
//  Created by Siddharth Shekar on 2/4/22.
//

import UIKit

class NewsTextTableViewCell: UITableViewCell {


    static let identifier = "NewsTextTableViewCell"
    
    private var newsTitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 20, weight: .semibold)
        
    
        return label
    }()
    
    private var newsSubTitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 16, weight: .light)
        label.textColor = .gray
        return label
    }()
    
        
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style,
                   reuseIdentifier: reuseIdentifier)
    
        addSubViews()
        
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
        
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        newsTitleLabel.text = nil
        newsSubTitleLabel.text = nil
    }
    
    
    
    func configure(with viewModel: NewsTableViewCellViewModel ){
        
        
        newsTitleLabel.text = viewModel.title
        newsSubTitleLabel.text = viewModel.subTitle
        
        
    } // configure


    func addSubViews(){
               
        
        contentView.addSubview(newsTitleLabel)
        contentView.addSubview(newsSubTitleLabel)
        
    
        newsTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        newsTitleLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10).isActive = true
        newsTitleLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 5).isActive = true
        newsTitleLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -5).isActive = true

        newsSubTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        newsSubTitleLabel.topAnchor.constraint(equalTo: newsTitleLabel.bottomAnchor, constant: 10).isActive = true
        newsSubTitleLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 5).isActive = true
        newsSubTitleLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -5).isActive = true

        self.contentView.bottomAnchor.constraint(equalTo: newsSubTitleLabel.bottomAnchor, constant: 10).isActive = true
        
        
    }
    
    
    

}
