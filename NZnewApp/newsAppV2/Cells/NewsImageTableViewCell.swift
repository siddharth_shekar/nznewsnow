//
//  NewsTableViewCell.swift
//  newsAppV2
//
//  Created by Siddharth Shekar on 1/26/22.
//

import UIKit


class NewsImageTableViewCell: UITableViewCell {

    static let identifier = "NewsImageTableViewCell"
    let stackView = UIStackView()
    
    private var newsTitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 20, weight: .semibold)
        
    
        return label
    }()
    
    private var newsSubTitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 16, weight: .light)
        label.textColor = .gray
        return label
    }()
    
    
    private let newsImageView: UIImageView = {
        
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.animationRepeatCount = 1
       
        return imageView
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style,
                   reuseIdentifier: reuseIdentifier)
        

        addSubViews()
        
        
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
        
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        newsTitleLabel.text = nil
        newsSubTitleLabel.text = nil
        newsImageView.image = nil
        
    }
    
    
    
    
    func configure(with viewModel: NewsTableViewCellViewModel ){
        
        // image
        
        if viewModel.imageURL != URL(string: "NoImage"){
            
            if let data = viewModel.imageData{
                
                newsImageView.image = UIImage(data: data)
                
            }else if let url = viewModel.imageURL{
                
                // fetch the image
                // The [] is a capture
                URLSession.shared.dataTask(with: url){ data, _, error in
                    
                    guard let data = data, error == nil else {
                        return
                    }
                    
                    // if image is available get the image
                    viewModel.imageData = data
                    
                    // Or load the image from the url
                    DispatchQueue.main.async {
                        
                        self.newsImageView.image = UIImage(data: data)
                        self.imageView?.animationRepeatCount = 0
 
                    }
                    
                }.resume()
                
            }
        }
        
        
        newsTitleLabel.text = viewModel.title
        newsSubTitleLabel.text = viewModel.subTitle
                
    } // configure


    func addSubViews(){
        
        
        contentView.addSubview(newsImageView)
        contentView.addSubview(newsTitleLabel)
        contentView.addSubview(newsSubTitleLabel)
        
        newsImageView.translatesAutoresizingMaskIntoConstraints = false
        newsImageView.heightAnchor.constraint(equalToConstant: 180).isActive = true

        //newsImageView.topAnchor.constraint(equalToSystemSpacingBelow: self.contentView.bottomAnchor, multiplier: 0.20).isActive = true
        newsImageView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 20).isActive = true
        newsImageView.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor).isActive = true
        
        newsTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        newsTitleLabel.topAnchor.constraint(equalTo: newsImageView.bottomAnchor, constant: 20).isActive = true
        newsTitleLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 5).isActive = true
        newsTitleLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -5).isActive = true

        newsSubTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        newsSubTitleLabel.topAnchor.constraint(equalTo: newsTitleLabel.bottomAnchor, constant: 20).isActive = true
        newsSubTitleLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 5).isActive = true
        newsSubTitleLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -5).isActive = true
        
        self.contentView.bottomAnchor.constraint(equalTo: newsSubTitleLabel.bottomAnchor, constant: 20).isActive = true
        
        
        
    }
    
    
}// class
