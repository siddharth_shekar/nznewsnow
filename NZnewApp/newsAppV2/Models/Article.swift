//
//  Article.swift
//  newsAppV2
//
//  Created by Siddharth Shekar on 1/26/22.
//

import Foundation

struct NewsData: Codable{
    let status: String?
    let totalResults: Int?
    let articles: [Article]
}


struct Article: Codable{
    let source: Source
    let title: String
    let description: String?
    let url: String?
    let urlToImage: String?
    let publishedAt: String
    
}

struct Source: Codable{
    
    let name: String
}
