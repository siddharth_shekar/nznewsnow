//
//  TableViewCellViewModel.swift
//  newsAppV2
//
//  Created by Siddharth Shekar on 1/26/22.
//

import Foundation


class NewsTableViewCellViewModel{
    
    let title: String
    let subTitle: String
    let imageURL: URL?
    var imageData: Data? = nil
    
    
    init(title: String,
         subTitle: String,
         imageURL: URL?
        ){
        
        self.title = title
        self.subTitle = subTitle
        self.imageURL = imageURL

    }
}
