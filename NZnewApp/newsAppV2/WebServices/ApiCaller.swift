//
//  ApiCaller.swift
//  newsAppV2
//
//  Created by Siddharth Shekar on 1/26/22.
//

import Foundation
import UIKit


// key = 660fe4700bd34641aa93bcc5b3ed38ee

struct ApiRespose: Codable{
    
    let articles: [Article]
    
}

final class ApiCaller{
    
    static let shared = ApiCaller()
    
    struct Constants{
        
        static let topHeadlinesURL = URL(string: "https://newsapi.org/v2/top-headlines?country=us&apiKey=660fe4700bd34641aa93bcc5b3ed38ee")
        
    }
    
    func getPageNumber(page: Int, pageSize: Int, country: String)->String?{
        
        // For all news
        var urlString =  "https://newsapi.org/v2/top-headlines?country=\(country)&pagesize=\(pageSize)&page=\(page)&apiKey=660fe4700bd34641aa93bcc5b3ed38ee"
        
        // All News without COVID in the news
        //var urlString = "https://newsapi.org/v2/everything?q=-covid&pagesize=\(pageSize)&page=\(page)&apiKey=660fe4700bd34641aa93bcc5b3ed38ee"
        
        // All news from newshub, stuff and rnz without COVID or OMICRON in the headline
        /*
        var urlString = "https://newsapi.org/v2/everything?q=-coronavirus%20AND%20-omicron%20AND%20-covid&domains=newshub.co.nz,stuff.co.nz,rnz.co.nz&apiKey=660fe4700bd34641aa93bcc5b3ed38ee&pagesize=\(pageSize)&page=\(page)"
        */
        
        return urlString
        
    }
    
    private init(){}
    
    public func getTopStories(page: Int, pageSize: Int, country: String, completion: @escaping(Result<NewsData, Error>) -> Void){
        
        // For guard let the assigned variable needs to be an optional
        guard let urlString = getPageNumber(page: page,pageSize: pageSize,country: country), // forced unwrap
        
        let url = URL(string: urlString)
        else {  return  }
        
        // can return data, response or an error
        let task = URLSession.shared.dataTask(with: url) { data, _, error in
            
            // if error
            if let error = error{
                completion(.failure(error))
            }
            
            // if we get data
            if let data = data{
                
                do {
                    
                    // data type to return is ApiResponse (a struct defined above)
                    // ApiResponse return an array of articles
                    // Stores the array in some variable called res
                    let res = try JSONDecoder().decode(NewsData.self, from: data)
                   
                    // On success return the array of articles
                    completion(.success(res))

                }
                catch{
                    completion(.failure(error))
                }
            }
        }
            
        task.resume()
        
        }
        
    }// class
