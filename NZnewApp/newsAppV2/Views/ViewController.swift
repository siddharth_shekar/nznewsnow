//
//  ViewController.swift
//  newsAppV2
//
//  Created by Siddharth Shekar on 1/26/22.
//

import UIKit

import SafariServices


import GoogleMobileAds
import StoreKit


class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SKPaymentTransactionObserver {
    

    private let tableView: UITableView = {
        let tableView = UITableView()

        tableView.register(NewsImageTableViewCell.self, forCellReuseIdentifier: NewsImageTableViewCell.identifier)
        tableView.register(NewsTextTableViewCell.self, forCellReuseIdentifier: NewsTextTableViewCell.identifier)

        return tableView
    }()
    
    
    var newsTVCellVMs = [NewsTableViewCellViewModel]()
    var articles = [Article]()
    let pageSize: Int = 10
    let country = "nz"
    var currentPage = 1
    var totalPageCount: Int = 0
    var fetchingDataInProgress = false
    var totalNewsArticles: Int?
    
    let refreshControl = UIRefreshControl()

    var bannerView: GADBannerView!
    
    var footerView: UIView?
    var removeAdButton: UIButton?
    var restoreAdButton: UIButton?
    
    var stackView = UIStackView()
    
    let iapProductId = "nznewsnowRemoveAd"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        title = "NZNEWSNOW"
        
        
        SKPaymentQueue.default().add(self)
        
        
        // Add Table view
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull To Refresh")
        self.refreshControl.addTarget(self, action: #selector(reloadPage), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        fetchData(page: 1)
        
        if !isPurchased(){
            
            
            // Add uibutton view
            removeAdButton = UIButton(type: UIButton.ButtonType.roundedRect)
            removeAdButton?.setTitle("RemoveAd", for: UIControl.State.normal)
            removeAdButton?.backgroundColor = .white
            removeAdButton?.isOpaque = false
            removeAdButton?.layer.borderWidth = 2
            removeAdButton?.layer.borderColor = CGColor(red: 66.0/256.0, green: 135.0/256.0, blue: 245.0/256.0, alpha: 1)
            
            removeAdButton?.addTarget(self, action: #selector(onRemoveAdButtonClicked), for: .touchUpInside)
            
            
            restoreAdButton = UIButton(type: UIButton.ButtonType.roundedRect)
            restoreAdButton?.setTitle("Restore", for: UIControl.State.normal)
            restoreAdButton?.backgroundColor = .white
            restoreAdButton?.isOpaque = false
            restoreAdButton?.layer.borderWidth = 2
            restoreAdButton?.layer.borderColor = CGColor(red: 66.0/256.0, green: 135.0/256.0, blue: 245.0/256.0, alpha: 1)
            restoreAdButton?.addTarget(self, action: #selector(onRestoreButtonClicked), for: .touchUpInside)
            

            stackView.axis = .horizontal
            stackView.distribution = .fillEqually
            stackView.alignment = .center
            //stackView.spacing = 8
            stackView.contentMode = .scaleToFill
            stackView.autoresizesSubviews = true
            stackView.clearsContextBeforeDrawing = true
            
            
            view.addSubview(stackView)
            
            stackView.translatesAutoresizingMaskIntoConstraints = false
            stackView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
            stackView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
            stackView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
            
            stackView.addArrangedSubview(removeAdButton!)
            stackView.addArrangedSubview(restoreAdButton!)
            
            removeAdButton?.translatesAutoresizingMaskIntoConstraints = false
            removeAdButton?.heightAnchor.constraint(equalToConstant: 50).isActive = true
            //removeAdButton?.widthAnchor.constraint(equalToConstant: view.frame.size.width/2).isActive = true
 
            
            restoreAdButton?.translatesAutoresizingMaskIntoConstraints = false
            restoreAdButton?.heightAnchor.constraint(equalToConstant: 50).isActive = true
            //restoreAdButton?.widthAnchor.constraint(equalToConstant: view.frame.size.width/2).isActive = true
            
            
            // Add Banner view
            bannerView = GADBannerView(adSize: GADCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(view.frame.size.width))
            bannerView.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(bannerView)
            
            bannerView.adUnitID = "ca-app-pub-3896672990290126/2734507806"
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
            
            tableView.addSubview(bannerView)
            
            bannerView.translatesAutoresizingMaskIntoConstraints = false
            bannerView.bottomAnchor.constraint(equalTo: stackView.topAnchor).isActive = true
            
        }
    }
    

    
    
   @objc func reloadPage(){
        if !fetchingDataInProgress{
            

            currentPage = 1
            articles.removeAll()
            fetchData(page: currentPage)
        }
    }
    
    @objc func onRemoveAdButtonClicked(_ sender: Any){
       
        print("RemoveAd Button Pressed")
        
        buyButtonPressed()
    }
    
    @objc func onRestoreButtonClicked(_ sender: Any){
        
        print("Restore Button Pressed")
        
        
        restoreButtonPressed()
    }
    
    
    func buyButtonPressed(){
        
        // Check if user can make purchases
        if SKPaymentQueue.canMakePayments(){
            
            // Make new Payment Request
            let paymentRequest = SKMutablePayment()
            
            // Set product identifier to our product id
            paymentRequest.productIdentifier = iapProductId
            
            // Add payement request to the payment queue
            SKPaymentQueue.default().add(paymentRequest)
            
        }else{
            print("User Cant Make Payments")
        }
    }
    
    func restoreButtonPressed(){
        
        
        SKPaymentQueue.default().restoreCompletedTransactions()
        
    }
    
    func removeAd(){
        
        UserDefaults.standard.set(true, forKey: iapProductId)
        bannerView.removeFromSuperview()
        
        self.navigationController?.setToolbarHidden(true, animated: true)
        
        removeAdButton?.removeFromSuperview()
        restoreAdButton?.removeFromSuperview()

    }
    
    func isPurchased()-> Bool{
        
        let purchaseStatus = UserDefaults.standard.bool(forKey: iapProductId)
                
        let status = purchaseStatus ? true : false
        
        return status
        
    }
    
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        for transaction in transactions {
            
            if transaction.transactionState == .purchased{
                
                // User payment successfule
                print("Transaction Successful")
                
                //
                removeAd()
                
                // remove current transaction from the queue
                SKPaymentQueue.default().finishTransaction(transaction)
                
            }else if transaction.transactionState == .failed{
                
                //Payement Failed
                print("Transaction Failed")
                
                if let error  = transaction.error{
                    let errorDescription = error.localizedDescription
                    print("Transaction Failed Due to Error\(errorDescription)")
                }
                
                // remove current transaction from the queue
                SKPaymentQueue.default().finishTransaction(transaction)
                
            }else if transaction.transactionState == .restored{
                
                removeAd()
                
                print("Transaction restored")
                
                SKPaymentQueue.default().finishTransaction(transaction)
                
            }
        }
        
        
        
    }
    

    
    func fetchData(page: Int){
        
        fetchingDataInProgress = true
        
        ApiCaller.shared.getTopStories(page: page, pageSize: pageSize, country: country){ [weak self] result in
            
            switch result{
                
            case .success(let newsData):
                // Stores articles in the viewModels array
                

                self?.articles.append(contentsOf: newsData.articles)
                
                self?.totalNewsArticles = newsData.totalResults
                
                if let pagesSize = self?.pageSize{
                    if let result = newsData.totalResults{
            
                        self?.totalPageCount = result % pagesSize != 0 ? ( result / pagesSize ) + 1 : ( result / pagesSize )
                        
                    }
                }
                
                
                self?.newsTVCellVMs = (self?.articles.compactMap({
                    NewsTableViewCellViewModel(
                        title: $0.title,
                        subTitle: $0.description ?? "",
                        imageURL: URL(string: $0.urlToImage ?? "")
                    )
                }))!
                

                DispatchQueue.main.async {
                    
                    self?.fetchingDataInProgress = false
                    self?.tableView.reloadData()
                    self?.refreshControl.endRefreshing()
                }
                
            case .failure(let error):
                print(error)
                
            }
        }
        
    }
    
    
    
    // table delegate functions
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return newsTVCellVMs.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let temp = "\(String(describing: newsTVCellVMs[indexPath.row].imageURL))"
        
        print("urllength: \(temp.count)")
        print(temp)
        
        if temp.count <= 3 {
            
            guard let cell = tableView.dequeueReusableCell(
                withIdentifier: NewsTextTableViewCell.identifier,
                for: indexPath) as? NewsTextTableViewCell else{
                    
                    fatalError()
                }
            
            cell.configure(with: newsTVCellVMs[indexPath.row])
    
            cell.sizeToFit()
            cell.layoutIfNeeded()
            
            return cell
            
        } else{

            guard let cell = tableView.dequeueReusableCell(
                withIdentifier: NewsImageTableViewCell.identifier,
                for: indexPath) as? NewsImageTableViewCell else{

                    fatalError()
                }
            cell.configure(with: newsTVCellVMs[indexPath.row])

            cell.sizeToFit()
            cell.layoutIfNeeded()

            return cell


        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
        tableView.deselectRow(at: indexPath, animated: true)
        
        let article = articles[indexPath.row]
        
        
        guard let url = URL(string: article.url ?? "") else{
            
            return
        }
        
        let vc = SFSafariViewController(url: url)
        present(vc, animated: true)
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return UITableView.automaticDimension

    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let position = scrollView.contentOffset.y
        
        if position > (tableView.contentSize.height - 50 - scrollView.frame.height)  {
            
            if !fetchingDataInProgress{
                
                currentPage += 1
                
                    if currentPage <= totalPageCount{
                        
                        fetchData(page: currentPage)
                    }
                
            }
        }
    }
    
    

    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 25
    }
    
} // class

